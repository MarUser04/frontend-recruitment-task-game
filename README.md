# Frontend UI Recruitment task

Link live: https://frontend-dev-task.vercel.app/

# Steps to run the project:

1- Run **yarn install** or **npm install** to install dependencies.

2- After installing dependencies, start the project with **yarn start** or **npm run start**.
