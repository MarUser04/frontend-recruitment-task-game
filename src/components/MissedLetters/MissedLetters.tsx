import React from "react";
import "./MissedLetters.scss";

const MissedLetters: React.FC<{ missedLetters: string[] }> = ({
  missedLetters,
}) => {
  return (
    <div className="missedLettersContainer">
      <h2>YOU MISSED:</h2>
      <div className="letters">
        {missedLetters &&
          missedLetters.map((letter, index) => (
            <span key={index}>{letter}</span>
          ))}
      </div>
    </div>
  );
};

export default MissedLetters;
