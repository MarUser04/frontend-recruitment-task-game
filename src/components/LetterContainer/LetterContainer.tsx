import React from "react";
import "./LetterContainer.scss";

const LetterContainer: React.FC<{ letter: string; active: boolean }> = ({
  letter,
  active,
}) => {
  return (
    <div
      className="letterBox"
      style={{ background: letter ? "gray" : "lightgray" }}
    >
      <span style={{ display: active ? "block" : "none" }}>{letter}</span>
    </div>
  );
};

export default LetterContainer;
