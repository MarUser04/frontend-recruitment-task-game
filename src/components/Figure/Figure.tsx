import React from "react";
import "./Figure.scss";

import Bar from "../../assets/images/bar.png";
import Head from "../../assets/images/head.png";
import Neck from "../../assets/images/neck.png";
import Body from "../../assets/images/corpus.png";
import RightArm from "../../assets/images/right-arm.png";
import LeftArm from "../../assets/images/left-arm.png";
import RightHand from "../../assets/images/right-hand.png";
import LeftHand from "../../assets/images/left-hand.png";
import RightLeg from "../../assets/images/right-leg.png";
import LeftLeg from "../../assets/images/left-leg.png";
import RightFoot from "../../assets/images/right-foot.png";
import LeftFoot from "../../assets/images/left-foot.png";

const Figure: React.FC<{ step: number }> = ({ step }) => {
  return (
    <div className="figureContainer">
      <img src={Bar} alt="Bar" className="figureContainer__figureBar" />
      <div className="figureContainer__figureBody">
        <img
          src={Head}
          alt="Head"
          style={{ display: step > 0 ? "block" : "none" }}
        />
        <img
          src={Neck}
          alt="Neck"
          style={{ display: step > 1 ? "block" : "none" }}
        />
        <img
          src={Body}
          alt="Body"
          style={{ display: step > 2 ? "block" : "none" }}
        />
        <img
          src={RightArm}
          alt="RightArm"
          style={{ display: step > 3 ? "block" : "none" }}
        />
        <img
          src={LeftArm}
          alt="LeftArm"
          style={{ display: step > 4 ? "block" : "none" }}
        />
        <img
          src={RightHand}
          alt="RightHand"
          style={{ display: step > 5 ? "block" : "none" }}
        />
        <img
          src={LeftHand}
          alt="LeftHand"
          style={{ display: step > 6 ? "block" : "none" }}
        />
        <img
          src={RightLeg}
          alt="RightLeg"
          style={{ display: step > 7 ? "block" : "none" }}
        />
        <img
          src={LeftLeg}
          alt="LeftLeg"
          style={{ display: step > 8 ? "block" : "none" }}
        />
        <img
          src={RightFoot}
          alt="RightFoot"
          style={{ display: step > 9 ? "block" : "none" }}
        />
        <img
          src={LeftFoot}
          alt="LeftFoot"
          style={{ display: step > 10 ? "block" : "none" }}
        />
      </div>
    </div>
  );
};

export default Figure;
