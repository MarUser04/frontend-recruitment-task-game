import React, { useEffect, useState, useCallback } from "react";
import "./App.scss";
import Figure from "./components/Figure/Figure";
import LetterContainer from "./components/LetterContainer/LetterContainer";
import MissedLetters from "./components/MissedLetters/MissedLetters";

const App = () => {
  const [step, setStep] = useState<number>(0);
  const [hits, setHits] = useState<number>(0);
  const [wordLength, setWordLength] = useState<number>(0);
  const [gameOver, setGameOver] = useState<boolean>(false);
  const [missedLetters, setMissedLetters] = useState<Array<string>>([]);
  const [letterArray, setLetterArray] = useState<
    Array<{ value: string; status: boolean }>
  >([]);

  const handleKeyDown = useCallback(
    (event: any) => {
      if (
        ((event.keyCode > 64 && event.keyCode < 91) ||
          (event.keyCode > 96 && event.keyCode < 123)) &&
        step < 11 &&
        hits < wordLength
      ) {
        let updateArray = letterArray;
        let update = false;
        updateArray.forEach((item) => {
          if (item.value === event.key.toUpperCase() && !item.status) {
            item.status = true;
            setHits((prevState: number) => prevState + 1);
            update = true;
          } else if (item.value === event.key.toUpperCase() && item.status) {
            update = true;
          }
        });

        if (update) {
          setLetterArray([...updateArray]);
        } else {
          if (!missedLetters.includes(event.key.toUpperCase())) {
            setMissedLetters((missedLetters) => [
              ...missedLetters,
              event.key.toUpperCase(),
            ]);
            setStep((prevState: number) => prevState + 1);
            if (step === 10) {
              setGameOver(true);
            }
          }
        }
      }
    },
    [step, missedLetters, letterArray, hits, wordLength]
  );

  useEffect(() => {
    window.addEventListener("keydown", handleKeyDown);
    return () => {
      window.removeEventListener("keydown", handleKeyDown);
    };
  }, [handleKeyDown]);

  useEffect(() => {
    if (hits > 0 && hits === wordLength) {
      setGameOver(true);
    }
  }, [hits, wordLength]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const response = await (
      await fetch("https://random-word-api.herokuapp.com/word?number=1")
    ).json();

    let arrayWord = [];
    setWordLength(response[0].length);
    for (let letter of response[0]) {
      arrayWord.push({
        value: letter.toUpperCase(),
        status: false,
      });
    }
    if (arrayWord.length < 11) {
      for (let i = arrayWord.length; i < 11; i++) {
        arrayWord.unshift({
          value: "",
          status: false,
        });
      }
    }
    setLetterArray([...arrayWord]);
  };

  const resetGame = () => {
    setHits(0);
    setStep(0);
    setWordLength(0);
    setMissedLetters([]);
    setLetterArray([]);
    fetchData();
    setGameOver(false);
  };

  return (
    <div className="main">
      <div
        className="main__figure"
        style={{ height: letterArray.length > 12 ? "600px" : "" }}
      >
        <Figure step={step} />
        <MissedLetters missedLetters={missedLetters} />
      </div>
      <div
        className="main__letters"
        style={{ justifyContent: letterArray.length === 0 ? "center" : "" }}
      >
        {letterArray.length > 0 &&
          letterArray.map((letter, index) => (
            <LetterContainer
              key={index}
              letter={letter.value}
              active={letter.status}
            />
          ))}
        {letterArray.length === 0 && (
          <span className="main__loading">Loading</span>
        )}
      </div>
      <div className="main__blueBox"></div>
      {gameOver && (
        <div className="main__gameOver">
          <span>{step === 11 ? "GAME OVER" : "WINNING"}</span>
          <span onClick={() => resetGame()}>NEW WORD</span>
        </div>
      )}
    </div>
  );
};

export default App;
